#!/usr/bin/env bash

#variable for estimering av tidtabell
flag="$1"               #Forste argument som er flagget -E eller -W
depStation="$2"         #
declare -i lineCount=1  #Teller av linjer i inputfil
declare -a Subway=()    #Array som inneholder all informasjon om en t-bane

#Henter data de de forste avgangene pa t-banestopp.
if [ "$1" == "blindern" ] || [ "$2" == "blindern" ]; then
    timeTable=$(curl "http://mon.ruter.no/SisMonitor/Refresh?stopid=3010360&computerid=acba4167-b79f-4f8f-98a6-55340b1cddb3&isOnLeftSide=true&blocks=&rows=&test=&stopPoint=" | grep "td")
elif [ "$1" == "ensjo" ] || [ "$2" == "ensjo" ]; then
    timeTable=$(curl "http://mon.ruter.no/SisMonitor/Refresh?stopid=3011430&computerid=acba4167-b79f-4f8f-98a6-55340b1cddb3&isOnLeftSide=true&blocks=&rows=&test=&stopPoint=" | grep "td")
elif [ "$1" == "-h" ] || [ "$1" == "-help" ]; then
    printf "usage: ./subway.sh [--W or --E] [ensjo or blindern(shows forskningsparken by default)]\n"
    exit 0
else
    timeTable=$(curl "http://mon.ruter.no/SisMonitor/Refresh?stopid=3010370&computerid=acba4167-b79f-4f8f-98a6-55340b1cddb3&isOnLeftSide=true&blocks=&rows=&test=&stopPoint=" | grep "td")
fi
#
printf "$timeTable" > timeTable.txt

#Formaterer og klargjør for attributtnavn i tabell
clear
printf '%s\t%s\t%s\n' "Spor:" "Tid:" "Linje:"

#Laget en egen printemetode da formatering og betingelser ble voldsomt å ha med i whilelokken
printSubway(){
    if [ "$flag" == "--W" ] && [ "$9" == 2 ]; then
        printf '\t\t\ %10s\t\t%s\t%s%s\n' "$3" "$1" "$7" "$9"
    elif [ "$flag" == "--E" ] && [ "$9" == 1 ]; then
        printf '\t\t\ %10s\t\t%s\t%s%s\n' "$3" "$1" "$7" "$9"
    elif [ "$depStation" == "" ] && [ "$flag" != "--W" ] && [ "$flag" != "--E" ]; then
        printf '\t\t\ %10s\t\t%s\t%s%s\n' "$3" "$1" "$7" "$9"
    fi
}

#Looper gjennom html filen
while read -r line; do
    #Fjerner alle tager
    line=${line#*>}
    line=${line%<*}

    #All informasjonen ligger fordelt på 9 linjer.
    #Forst legger jeg alle linjer inn i Subwayarray.
    #Nar lineCount er storre enn 8 sendes arrayen til printemetoden
    if (( "$lineCount" > 8 )); then
        Subway=("${Subway[@]}" "$line")
        printSubway "${Subway[@]}"
        Subway=()
        lineCount=1
    else
        Subway=("${Subway[@]}" "$line")
        lineCount="$lineCount"+1
    fi
done < "timeTable.txt"

#Sletter timetable siden den ikke er nyttig lengre
rm timeTable.txt