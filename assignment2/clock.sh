#!/usr/bin/env bash

arg="$1"

currentTime(){
    if [ "$arg" == "--AMPM" ]; then
        date +%r
    else
        date +%T
    fi
}

while true
do
    clear
    currentTime
    sleep 1
done