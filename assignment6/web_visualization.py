from flask import Flask
from flask import render_template
from flask import request
import temperature_CO2_plotter as tcp

app = Flask(__name__)
tcp.read_csv()

@app.route("/")
def chart(from_year=1850, to_year=2010, month=10):
    """
    Gets info from url. param equals default values.
    Uses the temperature_CO2_plotter from 6.1 to get values.

    :param from_year:
    :param to_year:
    :param month:
    :return:
    """

    try:
        from_year = int(request.args.get('fromYear'))
        to_year = int(request.args.get('toYear'))
        month = int(request.args.get('month'))-1
    except TypeError:
        app.logger.info("None or misspelled args")
    #l and labels gets merged so that only a nice sequenze of labels shows
    l = [""] * (to_year - from_year + 1)
    step = 10 if (to_year - from_year) > 15 else 1
    labels = list(range(from_year, to_year, step))
    index = 0
    for label in labels:
        l[index] = label
        index += step
    #Gets the CO2 and the temperature values
    co2 = tcp.plot_CO2(from_year, to_year)
    temperature = tcp.plot_temperature(from_year, to_year, month)
    return render_template('chart.html', values=co2, values2=temperature, labels=l)

if __name__ == "__main__":
    app.run()