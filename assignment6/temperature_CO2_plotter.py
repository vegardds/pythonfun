import matplotlib.pyplot as plt

co2 = {}
temperature = {}

def read_csv():
    """
    Reads each line of the csv files. Assert them to co2 and temperature dictionaries
    :return:
    """
    for line in open('co2.csv', 'r'):
        values = line.split(',')
        co2[values[0]] = values[1].rstrip()

    for line in open('temperature.csv', 'r'):
        values = line.split(',')
        temperature[values[0]] = values[1:12] + [values[12].rstrip()]


def plot_CO2(from_year, to_year):
    """
    Plots all CO2 level based on range of from_year and to_year. Returns list of plotted values
    :param from_year:
    :param to_year:
    :return list:
    """
    co2_in_range = []
    
    for year in range(from_year, to_year + 1):
        co2_in_range = co2_in_range + [co2[str(year)]]
        
    return co2_in_range
    
def plot_temperature(from_year, to_year, month):
    """
    Plots all temperatures based on from_year, to_year and month. Returns list of plotted values
    :param from_year:
    :param to_year:
    :param month:
    :return list:
    """
    temperature_in_range = []

    # Loops through range of years to plot the temperature for each specific year
    for year in range(from_year, to_year + 1):
        temperature_in_range = temperature_in_range + [temperature[str(year)][month]]
    
    return temperature_in_range

def get_min_max_temperature(from_year, to_year, month):
    temp_list = plot_temperature(from_year, to_year, month)
    min_max_temp = {'min': min(temp_list), 'max': max(temp_list)}
    return min_max_temp

def get_min_max_CO2(from_year, to_year):
    CO2_list = plot_CO2(from_year, to_year)
    min_max_CO2 = {'min': min(CO2_list), 'max': max(CO2_list)}
    return min_max_CO2

if __name__ == "__main__":
    read_csv()
    try:
        from_year = int(input("Enter start year(integer): "))
        to_year = int(input("Enter last year(integer): "))
        month = int(input("Enter month to analyze temp(integer): "))
    except ValueError:
        print("Please insert integers only.")
        exit()

    co2_in_range = plot_CO2(from_year, to_year)
    temperature_in_range = plot_temperature(from_year, to_year, month)
    
    plt.subplot(211)
    plt.plot(list(range(from_year, to_year+1)), co2_in_range, 'ro')
    plt.axis([from_year-1, to_year+1, 0, 10000])

    plt.subplot(212)
    plt.plot(list(range(from_year, to_year+1)), temperature_in_range, 'ro')
    plt.plot([from_year, to_year], [temperature_in_range[0], temperature_in_range[-1]])
    plt.axis([from_year-1, to_year+1, -10, 25])
    plt.show()