#!/usr/bin/python
#Task 4.4 User interface
from sys import argv
import mandelbrot_1 as m1
import mandelbrot_2 as m2

try:
    import mandelbrot_3 as m3
    import mandelbrot_4 as m4
except ImportError:
    print("To run --cython or --swig, you must run 'make_module.sh'")

del argv[0]  # removes first element so it does'nt check itself twice
plot_filename = None

#the help flagged option
if len(argv) == 0 or argv[0] == "--help":
    print("usage python mandelbrot_ui.py [option] [args]...\n"
          "Options are as folows:\n"
          "--py     :Mandelbrot with normal python implementation (4.1).\n"
          "--numpy  :Mandelbrot with numpy implementation (4.2).\n"
          "--cython :Mandelbrot with Cython implementation (4.3).\n"
          "--swig   :Mandelbrot with SWIG implementation (4.4) ***NOT WORKING***.\n"
          "--art    :Gives the user options to generate various Mandelbrot art (4.6).\n"
          "Arguments are as follows:\n"
          "(doubles:) xmin xmax ymin ymax (integers:)width height\n"
          )
    exit(0)

def color_selection():
    print("Do you want to set the color scheme of the Mandelbrot set?(y/n)")
    if input(">") == "y":
        print("Type 1 for viridis, 2 for inferno or 3 for plasma")
        color_select=input(">")
        if color_select == "1":
            return "viridis"
        elif color_select == "2":
            return "inferno"
        elif color_select == "3":
            return "plasma"
        else:
            print("You did not chose one of the possible options. Color set to default")
            return None
    else:
        return None

if len(argv) == 8:
    plot_filename == argv[7]

if len(argv) == 7 or 8: #with filename
    try:
        if argv[0] == "--py":
            m1.compute_mandelbrot(float(argv[1]), float(argv[2]), float(argv[3]), float(argv[4]),
                                  int(argv[5]), int(argv[6]), plot_filename=plot_filename, color_scheme=color_selection()
                                  )
        elif argv[0] == "--numpy":
            m2.compute_mandelbrot(float(argv[1]), float(argv[2]), float(argv[3]), float(argv[4]),
                                  int(argv[5]), int(argv[6]), plot_filename=plot_filename, color_scheme=color_selection()
                                  )
        elif argv[0] == "--cython":
            m3.compute_mandelbrot(float(argv[1]), float(argv[2]), float(argv[3]), float(argv[4]),
                                  int(argv[5]), int(argv[6]), plot_filename=plot_filename, color_scheme=color_selection()
                                  )
        elif argv[0] == "--swig":
            m4.mandelbrot_set(float(argv[1]), float(argv[2]), float(argv[3]), float(argv[4]),
                              int(argv[5]), int(argv[6])
                              )
    except ValueError:
        print("Wrong use of arguments")
    except NameError:
        print("Make sure you use the correct datatypes")
        exit(1)
elif argv[0] == "--art":
    print('You can choose to generate the twist(t), the devils tree(dt) or The Mandelbrot glory(mg)')
    selection = input('Type: t, dt or mg >')
    if selection == 't':
        m3.compute_mandelbrot(-0.160568374422, -0.160567557389, 1.037894847008, 1.037895462347,
                              1000, 1000, plot_filename="The_twist", color_scheme="hot")
    elif selection == 'dt':
        m3.compute_mandelbrot(0.0, 0.015, 0.775, 0.790, 1000, 1000,
                              plot_filename="The_devils_tree", color_scheme="hot")
    elif selection == 'mg':
        m3.compute_mandelbrot(-0.16240739030192, -0.16240717646344, 1.0409347340878, 1.0409348946,
                              1000, 1000, plot_filename="The_Mandelbrot_glory", color_scheme="summer")
    else:
        print("You did not choose one of the art options. Program exiting")
        exit(0)
else:
    print("Error: Wrong number of arguments. Try mandelbrot_ui.py --help")