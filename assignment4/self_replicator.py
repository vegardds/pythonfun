#!/usr/bin/env python
import inspect
import self_replicator

source_code = inspect.getsourcelines(self_replicator)

def replicate():
    for line in source_code[0]:
        print(line)

replicate()