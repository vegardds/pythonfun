import mandelbrot_1 as m1
import mandelbrot_2 as m2

mandelbrot_implementations = [m1, m2]

def test_answer():
    assert test_implementations()

def test_implementations():
    for mi in mandelbrot_implementations:
        if not test_outside_mandelbrot_set(mi):
            return False

    for mi in mandelbrot_implementations:
        if not test_inside_mandelbrot_set(mi):
            return False
    return True

def test_outside_mandelbrot_set(self):
    onlyzeros = True
    matrix = self.compute_mandelbrot( 3, 4, 3, 4, 1000, 1000, max_escape_time=1)

    for Nx in matrix[0]:
        print(Nx)
        if Nx != 0:
            onlyzeros = False

    for Ny in matrix[1]:
        if Ny != 0:
            onlyzeros = False

    return onlyzeros

def test_inside_mandelbrot_set(self):
    nozeros = True
    matrix = self.compute_mandelbrot( 3, 4, 3, 4, 1000, 1000, max_escape_time=1)

    for Nx in matrix[0]:
        if Nx == 0:
            nozeros = False

    for Ny in matrix[1]:
        if Ny == 0:
            nozeros = False

    return nozeros