from distutils.core import setup
from Cython.Build import cythonize
from Cython.Distutils import build_ext

setup(
    cmdclass = {'build ext': build_ext},
    ext_modules=cythonize("mandelbrot_3.pyx"),
)