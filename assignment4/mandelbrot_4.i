/* file: mandelbrot_4.i */
%module mandelbrot_4
%{
/* Everything in the %{ }% block will be copied in the wrapper file.
   Here, we include C header files necessary to compile the interface
*/
#include "mandelbrot_4.h"
#include "python3.5/Python.h"
%}

/* list functions to be interfaced: */
int* mandelbrot_set (double xmin, double xmax, double ymin, double ymax, int width, int height, int *OUTPUT);
%include "mandelbrot_4.h"