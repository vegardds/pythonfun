Assignment 4 inf4331 vegardds

Acknolagements:
My answers on this assignment are strongly influenced by 'https://www.ibm.com/developerworks/community/blogs/jfp/entry/How_To_Compute_Mandelbrodt_Set_Quickly?lang=en'.
Although I had to understand what it did and I tweaked on the solutions as well.
I didn't get 4.4 to work properly due to my lack of C programing skills. For more on the topic see report4.txt.

Running the assignment
Build the Cython and SWIG implementation by using the file: ./make_module.sh
mandelbrot_ui.py is the user interface and answer to 4.5

Requirements
Requires pytest framework for unittesting

4.6: I assume that the function: compute_mandelbrot() is needed in every implementation of mandelbrot. setup.py is part of 4.6 is installed in the make_module.sh file. No unit test added.

Created using Ubuntu 16.04 (64 bit) and Anaconda3