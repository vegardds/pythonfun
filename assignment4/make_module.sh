#!/usr/bin/env bash

#Builds the cython implementation from 4.3
echo "Building cython implementation:"
python setup_3.py build_ext --inplace

#Builds the SWIG implementation from 4.4
echo "Buliding SWIG implementation:"
python setup_4.py build_ext
python setup_4.py install --install-platlib=.

#installing package
python setup.py install

#Unit test
pytest