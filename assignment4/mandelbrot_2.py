#!/usr/bin/env python
import numpy as np
import time
import matplotlib.pyplot as plt


def mandelbrot_set(xmin, xmax, ymin, ymax, width, height, maxiter):
    r1 = np.linspace(xmin, xmax, width)
    r2 = np.linspace(ymin, ymax, height)
    c = r1 + r2[:,None]*1j

    output = np.zeros(c.shape)
    z = np.zeros(c.shape, np.complex64)
    for it in range(maxiter):
        notdone = np.less(z.real * z.real + z.imag * z.imag, 4.0)
        output[notdone] = it
        z[notdone] = z[notdone] ** 2 + c[notdone]
    output[output == maxiter-1] = 0
    return output


def compute_mandelbrot(xmin, xmax, ymin, ymax, Nx, Ny, max_escape_time=1000, plot_filename=None, color_scheme="cool"):
    start = time.time()
    matrix = mandelbrot_set(xmin, xmax, ymin, ymax, Nx, Ny, max_escape_time)
    print('%.5f seconds' % (time.time() - start))

    plt.imshow(matrix, cmap=color_scheme)
    if plot_filename != None:
        plt.savefig(plot_filename)
    plt.show()
    print (matrix)
    return matrix
