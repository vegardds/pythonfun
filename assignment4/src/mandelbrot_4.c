/* File : mandelbrot_4.c */
#include <stdio.h>
#include "mandelbrot_4.h"

int
mandelbrot (double creal, double cimag) {
    float real = creal, imag = cimag;
    int n;
    for(n = 0; n < 1000; ++n) {
        float real2 = real*real;
        float imag2 = imag*imag;
        if (real2 + imag2 > 4.0)
            return n;
        imag = 2* real*imag + cimag;
    real = real2 - imag2 + creal;
    }
    return 0;
}



int*
mandelbrot_set (double xmin, double xmax, double ymin, double ymax, int width, int height, int *OUTPUT)
{
    int i,j;

    float *xlin = (float *) malloc (width*sizeof(float));
    float *ylin = (float *) malloc (width*sizeof(float));

    float    dx = (xmax - xmin)/width;
    float    dy = (ymax - ymin)/height;

    for (i = 0; i < width; i++)
        xlin[i] = xmin + i * dx;
    for (j = 0; j < height; j++)
        ylin[j] = ymin + j * dy;


    for (i = 0; i < width; i++) {
        for (j = 0; j < height; j++) {
          OUTPUT[i*width + j] = mandelbrot(xlin[i], ylin[j]);
        }
    }
    free(xlin);
    free(ylin);

    return OUTPUT;
}