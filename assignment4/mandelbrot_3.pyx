#!/usr/bin/env python
import cython as cython
import numpy as np
import time
import matplotlib.pyplot as plt

cdef int mandelbrot(double creal, double cimag, int maxiter):
    cdef:
        double real2, imag2
        double real = creal, imag = cimag
        int n

    for n in range(maxiter):
        real2 = real*real
        imag2 = imag*imag
        if real2 + imag2 > 4.0:
            return n
        imag = 2* real*imag + cimag
        real = real2 - imag2 + creal;
    return 0

cpdef mandelbrot_set(double xmin, double xmax, double ymin, double ymax, int width, int height, int maxiter):
    # type: (object, object, object, object, object, object, object) -> object
    cdef:
        double[:] r1 = np.linspace(xmin, xmax, width)
        double[:] r2 = np.linspace(ymin, ymax, height)
        long[:,:] n3 = np.empty((width,height), np.int)
        int i,j

    for i in range(width):
        for j in range(height):
            n3[j, i] = mandelbrot( r1[i], r2[j], maxiter)

    return n3

def compute_mandelbrot(xmin, xmax, ymin, ymax, Nx, Ny, max_escape_time=1000, plot_filename=None, color_scheme="cool"):
    start = time.time()
    matrix = mandelbrot_set(xmin, xmax, ymin, ymax, Nx, Ny, max_escape_time)
    print('%.5f seconds' % (time.time() - start))

    plt.imshow(matrix, cmap=color_scheme)
    if plot_filename != None:
        plt.savefig(plot_filename)
    plt.show()

    return matrix