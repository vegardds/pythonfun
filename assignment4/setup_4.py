from distutils.core import setup, Extension

name = "mandelbrot_4"      # name of the module
version = "1.0"  # the module's version number

setup(name=name, version=version,
      # distutils detects .i files and compiles them automatically
      ext_modules=[Extension(name='_mandelbrot_4', # SWIG requires _ as a prefix for the module name
                             sources=["mandelbrot_4.i", "src/mandelbrot_4.c"],
                             include_dirs=['src'])
])