#!/home/vegard/anaconda3/bin/python
import numpy as np
import matplotlib.pyplot as plt


def mandelbrot(z, maxiter):
    c = z
    for n in range(maxiter):
        if abs(z) > 2:
            return n
        z = z * z + c
    return maxiter


def mandelbrot_set(xmin, xmax, ymin, ymax, width, height, maxiter):
    r1 = np.linspace(ymin, ymax, width)
    r2 = np.linspace(xmin, xmax, height)
    output = []
    for r in r1:
        tmp = []
        for s in r2:
            tmp.append(mandelbrot(complex(s, r), maxiter))
        output.append(tmp)
    return output


def compute_mandelbrot(xmin, xmax, ymin, ymax, Nx, Ny, max_escape_time=1000, plot_filename=None):
    matrix = mandelbrot_set(xmin, xmax, ymin, ymax, Nx, Ny, max_escape_time)

    if plot_filename != None:
        plt.imshow(matrix)
        plt.savefig(plot_filename)
        plt.show()

    return matrix
