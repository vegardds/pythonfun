#!/usr/bin/env python
from distutils.core import setup
names=['mandelbrot_1', 'mandelbrot_2', 'mandelbrot_ui', 'self_replicator', 'test_mandelbrot']

for name in names:
    setup(name=name,
        version='1.0',
        py_modules=[name],       # modules to be installed
        scripts=[name + '.py'],  # programs to be installed
        )

setup(name='mandelbrot_3',
        version='1.0',
        py_modules=['mandelbrot_3'],   # modules to be installed
        scripts=['mandelbrot_3.pyx'],  # programs to be installed
        )