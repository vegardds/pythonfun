#!/usr/bin/env python
from sys import argv
from math import sin, sqrt, cos

stack = []
inVal = ""

while True:
    if len(argv) > 1:
        inVal = argv[1]
        argv.pop()
    else:
        inVal = raw_input(">")

    inVal = inVal.split(" ")
    for x in inVal:
        try:
            stack.append(int(x))
        except ValueError:
            try:
                if len(stack) > 1:
                    if x == '+':
                        stack.append(stack.pop() + stack.pop())
                    elif x == '-':
                        temp = stack.pop()
                        stack.append(stack.pop() - temp)
                    elif x == '*':
                        stack.append(stack.pop() * stack.pop())
                    elif x == '/':
                        temp = stack.pop()
                        stack.append(stack.pop() / temp)
                if x == 'v':
                    stack.append(sqrt(stack.pop()))
                elif x == 'sin':
                    stack.append(sin(stack.pop()))
                elif x == 'cos':
                    stack.append(cos(stack.pop()))
                elif x == "p":
                    print stack[-1]
                elif x == 'q':
                    exit(1)
            except IndexError:
                print "Stack empty"
