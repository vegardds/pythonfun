"""
This module unittests functions with with expected result
"""
class UnitTest(object):
    """Constructor"""
    def __init__(self, func, args, kwargs, res):    # make test
        self.func = func
        self.args = args
        self.kwargs = kwargs
        self.res = res

    """Returns if the function returns expected result and false if theres an exception"""
    def __call__(self):                             # run test
        try:
            return self.func(*self.args, **self.kwargs) == self.res
        except:
            return False