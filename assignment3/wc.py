#!/usr/bin/python
import glob
from sys import argv

def readFile(file):
    linecount = 0
    charcount = 0
    wordcount = 0

    for line in file:
        charcount += len(line)
        wordcount += len(line.split())
        if line[-1] == "\n":
            linecount += 1
    file.close();
    print "\t{0}\t{1}\t{2}\t{3}".format(linecount, wordcount, charcount, file.name)

if len(argv) > 1:
    del argv[0] #removes first element so it doesent check itself twice
    # Loop through args
    for arg in argv:
        #loops trough all files args referes to. For instance *.txt
        for filename in glob.glob(arg):
            file = open(filename, 'r')
            readFile(file)