#!/usr/bin/env python

import sys
import re

syntax = {}
theme = {}

def readSyntaxAndTheme(syntaxInput, themeInput):
    """
    Creates dictionaries based on syntax and theme
    :param syntaxInput:
    :param themeInput:
    :return void:
    """
    for line in syntaxInput:
        key = re.search('["](.*?)(": )', line)
        value = re.search('(: )(.*)', line)
        syntax.update({key.group(1): value.group(2)})

    for line in themeInput:
        key = re.search('^(.*?)(: )', line)
        value = re.search('(: )(.*)', line)
        theme.update({key.group(1): value.group(2)})


def printSyntaxHighlighting(fileToRead):
    """
    For each elem in syntax, replace the match with match nested in formatted color.

    :param fileToRead:
    :return:
    """
    codeToHighlight = open(fileToRead).read()
    for key, value in syntax.items():
        codeToHighlight = re.sub("(%s)" % key, "\033[{}m\\1\033[0m".format(theme.get(value)),
                                     codeToHighlight, flags=re.MULTILINE)
    print (codeToHighlight)

if len(sys.argv) == 1:
    print("Usage:\n>python3 highlighter.py [either] <somfile>.py <somfile>.sh OR <somfile>.sql \n>python3 highlighter.py <somfile>.syntax <somfile>.theme <somfile>")
elif len(sys.argv) == 4:
    try:
        readSyntaxAndTheme(open(sys.argv[1], 'r'), open(sys.argv[2], 'r'))
        printSyntaxHighlighting(sys.argv[3])
    except AttributeError:
        print("Something is wrong with args")
elif sys.argv[1][len(sys.argv[1]) - 3:] == ".py":  # hefekad
    readSyntaxAndTheme(open('python.syntax', 'r'), open('python.theme', 'r'))
    printSyntaxHighlighting(sys.argv[1])
elif sys.argv[1][len(sys.argv[1]) - 3:] == ".sh":  # hefekad
    readSyntaxAndTheme(open('favorite_language1.syntax', 'r'), open('favorite_language1.theme', 'r'))
    printSyntaxHighlighting(sys.argv[1])
elif sys.argv[1][len(sys.argv[1]) - 4:] == ".sql":  # hefekad
    readSyntaxAndTheme(open('favorite_language2.syntax', 'r'), open('favorite_language2.theme', 'r'))
    printSyntaxHighlighting(sys.argv[1])
else:
    print("Usage:\n>python3 highlighter.py [either] <somfile>.py <somfile>.sh OR <somfile>.sql \n>python3 highlighter.py <somfile>.syntax <somfile>.theme <somfile>".format())
