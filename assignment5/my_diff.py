#!/usr/bin/env python
import sys
import re

def diffChecker(originalText, modifiedText, output):
    """
    Organizes and formates files til lists with string elements (lines of file)
    Creats a list that has the longest common sequenze (ref lcs)
    Iterates through lcs, and prints everything that is replaced or added (ref findCommonLine)

    :param originalText:
    :param modifiedText:
    :param output:
    :return:
    """
    originalList = re.split('\n', originalText)
    modifiedList = re.split('\n', modifiedText)

    longestCommonSubsequence = re.split('\n', lcs(originalList, modifiedList))

    for lineLcs in longestCommonSubsequence:
        if (originalList != None):
            originalList = findCommonLine(lineLcs, originalList, "[-] ", output)
        if (modifiedList != None):
            modifiedList = findCommonLine(lineLcs, modifiedList, "[+] ", output)
        output.write("[0] " + lineLcs + "\n")


    output.close()




def findCommonLine(stringToCompare, list, prefix, outputfile):
    """
    Until common string is found, prefix gets printed out and first element of
    List gets removed.
    :param stringToCompare:
    :param list:
    :param prefix:
    :return list:
    """
    tmplist = list

    for element in list:
        if(stringToCompare == element):
            tmplist = tmplist[1:]
            return tmplist
        else:
            outputfile.write(prefix + element + "\n")
            tmplist = tmplist[1:]


def lcs(xstr, ystr):
    """
    Longest common subsequence.
    Finds the longest common sequence and creates a string.
    Enharited from: https://rosettacode.org/wiki/Longest_common_subsequence#Python
    :param xstr:
    :param ystr:
    :return string:
    """
    # Basecase
    if not xstr or not ystr:
        return ""
    # Removes elements to enable iteration
    x, xs, y, ys = xstr[0], xstr[1:], ystr[0], ystr[1:]
    #
    if x == y:
        return x + "\n" + lcs(xs, ys)
    else:
        return max(lcs(xstr, ys), lcs(xs, ystr), key=len)


diffChecker(open(sys.argv[1], 'r').read(), open(sys.argv[2], 'r').read(), open(sys.argv[3], 'w'))