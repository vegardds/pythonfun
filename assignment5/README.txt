Assignment5 by vegardds

5.1 to 5.4 is all in highlighter.py file.
In 5.3 name of files is favorite_language1.theme and favorite_language1.syntax. This is Bash script syntax highlighting
In 5.4 name of files is favorite_language2.theme and favorite_language2.syntax. This is SQL syntax highlighting

To execute 5.1 to 5.4, run:
>python3 highlighter.py [either] <somfile>.py <somfile>.sh OR <somfile>.sql
OR
>python3 highlighter.py <somfile>.syntax <somfile>.theme <somfile>

Example of bash script: ./example/calc.sh
Example of SQL script: ./example/select.sh

To execute diff(5.5), run:
>python3 my_diff.py <original>.txt <modified>.txt <outputfile>

To execute highlighting for diff(5.6), run:
>python3 highlighter.py diff.syntax diff.theme <outputfile>

Examples on original:
./example/en.txt
and modified:
./example/to.txt