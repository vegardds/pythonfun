#!/usr/bin/env bash

declare -i result;
re='^[0-9]+$'

option="$1"
shift;
result="$1"
shift;

declare -a Args="$@"

for arg in $Args; do
    if ! [[ $arg =~ $re ]]; then
        option="exit"
    fi

    case "$option" in
        S)
            result="$result"+"$arg";;
        P)
            result="$result"*"$arg";;
        m)
            if (( "$arg" < "$result" )); then
                result="$arg"
            fi;;
        M)
            if (( $arg > $result )); then
                result="$arg"
            fi;;
        *)
            printf "usage: ./calc.sh [SPmM] [int1 int2 ... intn]\n"; exit 1;;
    esac
done

echo "$result"